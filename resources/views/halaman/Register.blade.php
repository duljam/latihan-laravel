<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1> Buat Account Baru! </h1>
    <form action="/kirim" method="post">
        @csrf 
          <div>
          <h3> Sign Up form </h3>
          <label for="fname"> First name: </label><br><br>
            <input type="text" id="fname" name="awal"><br><br>
          <label for="lname"> Last name: </label><br><br>
            <input type="text" id="lname" name="akhir">
          <br><br>

          <label> Gender: </label><br><br>
            <input type="radio" name="gender" value="Male"> Male <br>
            <input type="radio" name="gender" value="Female"> Female <br>
            <input type="radio" name="gender" value="Other"> Other
          <br><br>

          <label> Nationality: </label><br><br>
            <select>
              <option> Indonesian </option>
              <option> America </option>
              <option> English </option>
            </select>
              <br><br>

            <label> Language Spoken: </label><br><br>
              <input type="checkbox" name="Language" value="id"> Bahasa Indonesia <br>
              <input type="checkbox" name="Language" value="en"> English <br>
              <input type="checkbox" name="Language" value="Other"> Other
            <br><br>

            <label> Bio: </label><br><br>
              <textarea rows="10" cols="35"></textarea><br>
            <input type="submit" value="Sign Up">
    </form>
</body>
</html>